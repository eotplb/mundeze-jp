# alfare

**alfare**(アルファーレ)は、マシューのアルフナント「ナピシュティム」に触発された論理的な表音文字で、mundezeを書くために特別に考案されたものです。文字の形と発音を一致させています。「茎」の位置と「腹」の位置はそれぞれ子音のモードと調音点を示しています。

![alfare_ja_another](alfare_ja_another.png)

## 調音

以下の表では調音された子音を種類と調音点によって分類しています。

![alfare_ja](alfare_ja.png)

線は調音方法(調音のために空気を排出する方法)の違いを、縦軸で表しています。

列は「腹」(文字のXの高さの部分)で表される、異なる調音場所(子音を調音するために空気を遮断する口の中の場所)を示しています。

alfareは大文字を使いませんが、**文字を大きく書く事による**大文字は認められています。

子音は全て長い縦軸で書き、有声音は全てループして書かれます。

## 文章の例

|||
|-|-|
|**ラテン文字**|**una linare a versara klame a ta mocey**<br>ola te danasko libri ni sami lu suconke ni moce.<br>ley avi logable ni sitse, ni muti kosadi o sereta menade.|
|**alfare**|![sample_text](http://mundezo.com/alfare/linare_alfare.gif)|
|**日本語訳**|**人権の普遍的宣言第一条**<br>すべての人間は生まれながらにして自由であり、尊厳と権利において平等である。<br>彼らは理性と良心に恵まれており、兄弟愛の精神をもってお互いに行動すべきである。|
