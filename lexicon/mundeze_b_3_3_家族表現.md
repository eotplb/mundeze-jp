# 家族表現

|||
|-|-|
|一族、家系|domanare|
|家族|familye|
|親 (父、母)|pe|
|母|mape|
|ママ|mama|
|父|bape|
|パパ|baba|
|息子、娘|ide|
|娘|maide|
|息子|baide|
|きょうだい|sere|
|兄弟|basere|
|姉妹|masere|
|兄、姉|presere|
|弟、妹|possere|
|兄|bapresere|
|姉|mapresere|
|弟|bapossere|
|妹|mapossere|
|甥、姪|yakide|
|甥|bayakide|
|姪|mayakide|

|||
|-|-|
|叔父、叔母|psere|
|叔父|bapsere|
|叔母|mapsere|
|いとこ|pyakide|
|従兄弟|bapyakide|
|従姉妹|mapyakide|
|祖父、祖母|prepe|
|祖母|maprepe|
|祖父|baprepe|
|母方の祖父|premape|
|父方の祖父|prebape|
|母方の祖父|bapremape|
|父方の祖母|maprebape|
|曾祖父、曾祖母|preprepe|
|孫息子、孫娘|poside|
|継親 (配偶者の親)|bope|
|継親 (親の新しい配偶者)|biunpe|
|義母 (配偶者の母)|mabope|
|義父 (親の新しい配偶者)|babiunpe|
|嫁、婿嫁|boide|
|義兄 (配偶者のきょうだい)|bosere|
|義姉 (配偶者のきょうだい)|biunsere|
