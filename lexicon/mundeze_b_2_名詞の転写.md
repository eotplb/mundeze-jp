# 名詞の転写

その単語の語源となる言語に関連して可能な限り正確な発音を維持するように努めています。

## 借用語

いくつかの国際的な単語(*casino, ski, sushi*…)は**品詞語尾を省略しても構いません**。

それらの単語を可能な限り正確な音韻で書き写して借用します(アクセントは最後の音節になります)。例えば *pizza* は pitse ではなく **pitsae** になりますが、これは pitsa と発音する事が出来ます(末尾が **‐e** である事を暗示しています)。

- katana (刀) ➜ katanai (刀を振る)
- djembe (djembé) ➜ djembei (ジャンベを演奏する)
- sovyet (ソビエト) ➜ sovyeta (ソビエトの)
- taksi (タクシー) ➜ taksii (タクシーに乗る)
- salsa (サルサ)) ➜ salsai (サルサを踊る)

## 名前

人名は元の言語で書かれたままにしておき、その後ろに角括弧でmundezeの発音に基づいた綴りを可能な限り正確に書きます。ラテン語の文字とは異なる文字で書かれた名前は音韻的に直接書き写されます。

- Friedrich Engels ➜ Friedrich Engels [fhithic engels]
- Albert Einstein ➜ Albert Einstein [albeht aynctayn]
- Rosa Luxemburg ➜ Rosa Luxemburg [roza luksembuhk]
- Michael Jackson ➜ Michael Jackson [maykel djakson]
- Che Guevara ➜ Che Guevara [tce gebara]
- Ivlivs Caesar (Julius Caesar) ➜ Iulius Caesar [yulyus kaesar]
- Ἀριστοτέλης (Aristotle) ➜ aristotelis
- Лев Троцкий (Leon Trotsky) ➜ lyev trotski
- جمال عبد الناصر حسين (Gamal Abdel Nasser Hussein) ➜ gemel abdel naser hoseyn
- 孔子 (Confucius) ➜ khong tsw
- ᠴᠢᠩᠭᠢᠰᠬᠠᠭᠠᠨ (Genghis Khan) ➜ tcingis han

(訳注:日本名をmundeze化する時は**禁則文字列**に注意)

- 太郎 (Tarô) ➜ taroo/tarou (訳注: **ow** は禁則文字列のため使えない)
- 竜之介 (Ryûnosuke) ➜ ryuunosuke
- 翔子 (Shôko) ➜ cooko/couko

## 国と住人

国名とその住人については、現地の名前に名詞の品詞語尾 **‐e** を付けて転記するだけで十分です。国名がそこに住む人々の名前に由来する場合、「国」の意味を持つ単語のどの部分(‐land, ‐stan…)も削除する必要があります。

一度このルールに従ってmundeze化された単語は国と住人の両方を指します。

- frans + **‐e** = franse = フランス、フランス人
- nipon + **‐e** = nipone = 日本、日本人

区別したい場合は、国は **‐ike**、住人は **‐ane** の接尾辞を使います。

- franse
	- frans**ike** = フランス
	- frans**ane** = フランス人
- nipone
	- nipon**ike** = 日本
	- nipon**ane** = 日本人

一部の国の名前はその国の住人を表す言葉に合わせている場合があります。例えばマダガスカルは、現地では *Madagasikara* で、それを元に **malagasike** と書きます。

[国名一覧](https://docs.google.com/spreadsheets/d/1CrCiJ8Gx60BMBPcTataGoxA9XeMH9AwLPKl445ysf9g/)

## 都市

都市名の転記も国名と同じ方法ですが、現地人(**‐ane**)を表す言葉を考慮する必要がある場合は**語尾が通常と異なる場合があります**。

例えば *Paris* は通常 **pari** と転記されますが、住人を表す *Parisian* に適合するように **pariz** と翻訳されます。そのためパリ、パリジャンは **pariz**, **parizane** と呼ばれます。

- Khartoum, Khartoumese ➜ hartum, hartumane
- Moscow, Muscovite ➜ moskwa, moskwaane
- Brussels, Brussels ➜ brusel, bruselane
- New York, New Yorker ➜ nuyork, nuyorkane
- Beijing, Beijingese ➜ peytcin, peytcinane

## 言語/方言

名称が国名に由来する場合は **国名の語根 + eze** で表します。地域や民族に由来する場合は **地域名の語根 + eze** で表します。中にはそれに当てはまらない転記もあります。

### 国名に由来

- French ➜ frans**eze**
- Chinese ➜ tcunk**eze**
- Russian ➜ rus**eze**
- Hungarian ➜ mayar**eze**

### 地域や民族に由来

- English ➜ ingl**eze**
- Catalan ➜ katal**eze**
- Basque ➜ euckal**eze**
- Cantonese ➜ kwanton**eze**
- Ainu➜ aynu**eze**
- Kurdish ➜ kurd**eze**
- Persian ➜ pars**eze**
- Wolof ➜ wolof**eze**

### その他･特殊

- Swahili ➜ swahili
- Hindi ➜ hindi
- Urdu ➜ urdu
- Lingala ➜ lingala

### 動詞化

(訳注:「話す(*speak*)」と「話**せる**(***can*** *speak*)」を区別しているのか不明のため敢えて両方を書いた)

- 私は英語を話します = me inglezi
	- 私は英語を話**せ**ます = me **abli** inglezi
- 私は英語とフランス語とスワヒリ語を話します = me ezi ingle, franse ni swahili
	- 私は英語とフランス語とスワヒリ語を話**せ**ます = me **abli** ezi ingle, franse ni swahili
- あなたは何の言語を話しますか? = kie tu ezi?
	- あなたは何の言語を話**せ**ますか? = kie tu **abli** ezi?
