# 知り合いになる

|くだけた表現|mundeze|本来の意味|
|-|-|-|
|やあ!|salam!|-|
|こんにちは!|bona die!|-|
|元気?|kio tu seti?|あなたは元気ですか?|
|ええ、あなたは?|me bono seti, ki tu?|私は元気です。あなたは?|
|私も。ありがとう|me nio, danke.|私もです。ありがとう|
|名前は?|kio tu nami?|あなたの名前は何ですか?|
|ケミ|me nami kemi.|私の名前はケミです|
|何歳? / いくつ?|kiom tu adji?|あなたは何歳ですか?|
|34歳|me terunti–kwar yaradji.|私は34歳です|
|何処から来たの?|da kien tu si?|何処から来ましたか?|
|ベニンから|me da benine.|私はベニンから来ました|
|どれ位になる?|da kios tu titieni?|ここに来てどれ位の期間が経ちましたか?|
|1週間|me titieni da un diseme.|私はここに来てから1週間滞在しています|
|どの位いるの?|ata kios tu deiti?|どの位ここに滞在しますか?|
|もう1週間|me duro deiti un diseme.|私はもう1週間滞在します|
|休み?|ki tu ferii?|あなたは休暇中ですか?|
|ここにいるのは好き?|ki tu ami titieni?|ここに滞在するのは好きですか?|
|また会える?|ki ebli noy rekofindi?|また会えますか?|
|悪い、やる事が沢山ある|me konfeli, me avi multa nudeye.|すみません、私にはする事が沢山あります|
|うん。これが私の電話番号|ha, tisi mea telefonnumere.|はい。これが私の電話番号です|
|またね!|tcaw!|-|
