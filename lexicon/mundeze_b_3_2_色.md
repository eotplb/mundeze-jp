# 色

基本的な色は以下の通りです。

- <font style="color:#ff0000">■</font> 赤 ➜ rodje
- <font style="color:#a52a2a">■</font> 茶 ➜ brune
- <font style="color:#ffa500">■</font> 橙 ➜ oranje
- <font style="color:#ffff00">■</font> 黄 ➜ jele
- <font style="color:#008000">■</font> 緑 ➜ verde
- <font style="color:#00ffff">■</font> 水色 ➜ syane
- <font style="color:#0000ff">■</font> 青 ➜ blue
- <font style="color:#800080">■</font> 紫 ➜ purpe
- <font style="color:#ff00ff">■</font> 赤紫 ➜ majente
- <font style="color:#ffc0cb">■</font> 桃 ➜ pinke
- <font style="color:#808080">■</font> 灰 ➜ grize
- <font style="color:#000000">■</font> 黒 ➜ kale
- <font style="color:#ffffff">■</font> 白 ➜ byele

これとは別に、**色相･明度･彩度**に基づいて色の名前を決める論理的なシステムを提唱します。

![色の表1](http://mundezo.com/img/kromo2d.png)

このシステムに従った主な色の名前は以下の通りです。

- <font style="color:#ff0000">■</font> 赤 ➜ rucye
- <font style="color:#a52a2a">■</font> 茶 ➜ ranye
- <font style="color:#ffa500">■</font> 橙 ➜ jocye
- <font style="color:#ffff00">■</font> 黄 ➜ jucye
- <font style="color:#00ff00">■</font> 黄緑 ➜ grucye
- <font style="color:#008000">■</font> 緑 ➜ grunye
- <font style="color:#00ffff">■</font> 水色 ➜ sucye
- <font style="color:#0000ff">■</font> 青 ➜ blucye
- <font style="color:#000080">■</font> 群青 ➜ blunye
- <font style="color:#800080">■</font> 紫 ➜ punye
- <font style="color:#ff00ff">■</font> 赤紫 ➜ pucye
- <font style="color:#808080">■</font> 灰 ➜ grinye

これら2つの値に加えて、80%未満の場合は中間音節を追加して飽和度を指定する事も出来ます。

![色の表2](http://mundezo.com/img/kromo3d.png)

- <font style="color:#e6b49a">■</font> 白い肌 ➜ ranucya
- <font style="color:#522e20">■</font> 黒い肌 ➜ ranunya

## 注意点

色を文章で表現する場合はともかく、文字に色を指定しても色覚障碍の人にはその色を認識出来ない場合があります。健常者には違う色に見えても、色覚障碍の人には色の区別が付かない場合もあります。**色を認識出来る事を前提としたデザインにしないよう気を付けましょう**。

- [Google Chrome 拡張 Colorblindly](https://chrome.google.com/webstore/detail/colorblindly/floniaahmccleoclneebhhmnjgdfijgg)
