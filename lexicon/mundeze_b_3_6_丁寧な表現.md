# 丁寧な表現

|||
|-|-|
|はい|ha|
|いいえ|ne|
|やあ|salam|
|こんにちは (全日)|bona die|
|おはよう|bona diese|
|こんにちは|bona difine|
|こんばんは|bona kalose|
|お休みなさい / 良い夢を|bona gofe / bono soní|
|またね / じゃあね|tcaw|
|さようなら / また今度|ata rekowe|
|お元気ですか?|kio tu seti?|
|何か変わった事は(ありましたか)?|kie novi?|
|どうぞ|ples|
|ここ / そこ|tisi / cí|
|ありがとう|danke / me danki tu|
|ありがとうございます / 本当にありがとう|gudanke|
|どういたしまして|ne dankibli / ne dankibla|
|ようこそ|gostibla / tu gostibli|
|お会い出来て/お目にかかれて光栄です|me felitci tsesi tu|
|どうぞお召し上がり下さい|bona nyame|
|ごめんなさい / すみません|konfela / me konfeli|
|お尋ねしますが / 失礼しますが / すみませんが|(me) sori|
|え、何て言った? / もう一度お願いします|e? kie? / ples repelí|
|もしもし (電話)|pela (+ 相手の名前)|
|おめでとう!|bono suní!|
|乾杯!|bono gulí!|
|お悔やみ申し上げます|konsike / me konsiki|
|お誕生日おめでとう|felitca naskodie|
|楽しい休日/休暇を<br>※メリークリスマスの代替表現にも使う|felitca ferie|
|明けましておめでとうございます|felitca reyare|
|親愛なる(名前/呼称/称号)|ibla done|
|わかりません|me ne tsi|
|(私には)理解出来ません / 訳が解らない|me ne mentcesi / me ne cesi|
|関係ありません / どうでもいい|ne gueti|
|私はあなたを愛しています|me lovi tu (恋愛相手) / me ami tu (一般的な愛情表現)|
|食事の準備が出来ました / 食べましょう|nyamose / nyamosi|
