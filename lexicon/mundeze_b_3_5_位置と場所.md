# 位置と場所

(訳注:明らかな間違いや食い違い、不足表現を加筆修正)

|||
|-|-|
|あなたは**何処に**にいますか?|**kien** tu?|
|私は**ここ**です|me **titieni**.|
|あなたは**そこ**です|tu **tieni**.|
|彼/彼女は**あそこ**です|lo **teltieni**.|
|あなたは**何処で**食べますか?|**kien** tu nyami?|
|私は**ここで**食べます|me nyami **titien**.|
|私は庭に**います**|me **eni** garde.|
|私は庭**で**食べます|me nyami **en** garde.|
|あなたは**何処に/へ**行くのですか?|na **kien** tu iti?|
|私は庭**に**行きます|me iti **na** garde.|
|私は庭の**周りを**移動しています|me iti **ru** garde.|
|あなたの出身は**何処**ですか?|da **kien** tu tiiti?|
|台所**から**来た所です|me teko tiiti **da** kukene.|
|鶏肉をオーブン(**内**)**で**調理しました|kokeye preo kukisi **in** warmere.|
|鶏肉はオーブンの**中です**|kokeye **ini** warmere.|
|ケーキをオーブン**から出したら**冷めます|keke fridesi **ek** warmere.|
|ケーキはオーブンの**外に**置いてあります|keke eni **ek** warmere.|
|ケーキがオーブン**から出ます**|keke **eki** warmere.|
|ケーキはテーブルの**上に**置いてあります|keke eni **sop** meze.|
|ケーキはテーブルの**上です**|keke **sopi** meze.|
|椅子はテーブルの**下に**置いてあります|sedile eni **sub** meze.|
|椅子はテーブルの**下です**|sedile **subi** meze.|
|私の寝室は家の**一番上(の階)に**あります|mea gofkamere eni **op** dome.|
|私の寝室は家の**一番上(の階)です**|mea gofkamere **opi** dome.|
|台所は家の**一番下(の階)に**あります|kukene eni **ub** dome.|
|台所は家の**一番下(の階)です**|kukene **ubi** dome.|
|私は母の**隣に**座っています|me sedi **laten** mea mape.|
|私はきょうだいと母の**間に**座っています|me sedi **mes** mea sere ni mea mape.|
|ケーキはテーブルの**中央辺りに**置かれています|keke eni **mes** meze.|
|ケーキはテーブルの**中央辺りです**|keke **mesi** meze.|
|私の家族が**家に**帰ってきました|mea familye jo tiiti **en mea dome**.|
|私の家族が**我が家に**食事をしに来てくれました|mea familye jo tiiti nyami **dom me**.|
|昨日私はパン屋**で**パンを買いました|me predio pravesi pane **en** panene.|
|昨日私はパン屋(**の家**)**で**寝ました|me predio gofi **dom** panane.|
|私は壁に**向かって**座っています|me sedi **tuk** mure.|
|姉の**前に**座っています|me sedi **preen** mea masere.|
|私達はテーブルを**囲んで**座っています|noy sedi **ru** meze.|
|私の実家が私の家の**側に**あります|mea pey jiti **laten** mea dome.|
|彼らは私の家の**近くに**住んでいます|ley jiti **ner** mea dome.|
|私の兄弟は**遠くに**住んでいます|mea serey jiti **tel** me.|
