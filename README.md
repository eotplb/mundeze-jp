# mundeze 日本語訳

ムンデゼ(mundeze)の[公式サイト](https://www.mundezo.com/en/)の日本語訳。原文のままでは日本人にとって理解が困難な場合は便宜文章を差し替えています。

## ライセンス

公式と同じ[クリエイティブコモンズ 表示-継承 3.0 非移植](https://creativecommons.org/licenses/by-sa/3.0/deed.ja)です。

翻訳の質を保証しません。修正には可能な限り応じますが、**義務を負いません**。

## PDF版

[Dropbox](https://www.dropbox.com/sh/rgdj4ilqulsfd4s/AABEghVL6IaBAwWacVeU0RXba?dl=0)からダウンロードして下さい(随時更新)。