# 代名詞

## 人称代名詞

||一人称|二人称|三人称|四人称|
|:-:|:-:|:-:|:-:|:-:|
||私<br>私に|あなた<br>あなたに|彼/彼女/それ<br>彼/彼女に|(不特定)|
|**基本** 単数|me|tu|lo|ane|
|**基本** 複数|noy|voy|ley|ane|
||||||
|**男性** 単数|bame|batu|balo|ane|
|**男性** 複数|banoy|bavoy|baley|ane|
||||||
|**女性** 単数|mame|matu|malo|ane|
|**女性** 複数|manoy|mavoy|maley|ane|
||||||
|**全性** 単数|zame|zatu|zalo|ane|
|**全性** 複数|zanoy|zavoy|zaley|ane|

対象を特定する必要がある場合を除いて**男性代名詞と女性代名詞を使う必要はありません**。

人称代名詞の代わりに敬称名詞 **done**(様、伯爵、陛下、お嬢さん等) を使う事も出来ますが、文化的な配慮を必要とする場合を除いて推奨されません。

- me lovi tu = 私はあなたを愛します
- tu lovi lo = あなたは彼/彼女を愛します
- balo lovi me = 彼は私を愛します
- malo lovi me = 彼女は私を愛します
- orelí me = 私の話を**聞いて**
- ki tu voli longice? – bame? = あなたはドレスが欲しいのですか? – 男?
- nalcí lo na me = (それを)私に**頂戴**
- lo gukusti = (それは)高い(高価)です
- manoy voli samete = 私達(女性)は平等を望みます
- ane fari gubona pane en franse = ある人(パン職人)はフランスで素晴らしいパンを作る
- otí ley tien = (それを)そこに**置いて**

## 所有形容詞と目的格

人称代名詞に形容詞の品詞語尾 **‐a** を付ける事で表します。

||一人称|二人称|三人称|四人称|
|:-:|:-:|:-:|:-:|:-:|
|所有形容詞|私の|あなたの|彼/彼女の|(不特定)の|
|目的格|私を|あなたを|彼/彼女を|(不特定)を|
|**基本** 単数|mea|tua|loa|anea|
|**基本** 複数|noya|voya|leya|anea|
||||||
|**男性** 単数|bamea|batua|baloa|anea|
|**男性** 複数|banoya|bavoya|baleya|anea|
||||||
|**女性** 単数|mamea|matua|maloa|anea|
|**女性** 複数|manoya|mavoya|maleya|anea|
||||||
|**全性** 単数|zamea|zatua|zaloa|anea|
|**全性** 複数|zanoya|zavoya|zaleya|anea|

- me si voya amige = 私はあなた達の友達です
- noy si tua amigey = 私達はあなた達の友達です
- tia buke si mea = この本は私のものです
- loa vale jo terupisi = その価値は3倍だった
- 
- sis tu peli tua name na me, me peli mea na tu = もしあなたが(あなたの)名前を教えてくれたら、私の(名前)を(あなたに)教えます
- si mea probleme, ne tua = 私の問題であり、あなたの問題ではありません
- loa rodji = 彼/彼女/それは赤い

## 再帰代名詞

### 目的語に対して使う

再帰代名詞 **su** を使うか、再帰接頭辞 **su‐** を付ける事で人称代名詞を再帰代名詞にする事が出来ますが、**必須ではありません**。文脈によっては対象が明らかで使う必要がない事もあります。

(訳注:要するに目的語が生物なのにどの/誰の生物か指定されていない場合は不確定の第三者という事になる。日本語のように目的語から対象の生物を省略してもその対象が主語にはならない。但し文章中に登場人物が1人しかいない等、**他人が対象になる事があり得ない場合は再帰代名詞を省略出来る**)

- me safifi **su** fase = me safifi sume fase = 私は(**私の**)顔を洗っています
- balo depuci <u>**su**balo</u> en dome = 彼は自宅で(<u>彼**自身**を</u>)描いています
- 
- ley safifi ley = 彼らは(彼ら自身以外の)彼らを洗います
	- 彼の所有格が示されていないため、誰の彼か判らない
- ley safifi **su** = 彼らは(彼ら**自身**を)洗います
- lo pediti kon <u>**su**loa</u> bape ni <u>**su**loa</u> wawe = その人は(<u>その人の</u>)父と(<u>その人の所有する</u>)犬と共に歩いています
- lo pediti kon <u>**su**loa</u> bape ni loa wawe = その人は(<u>その人の</u>)父と(誰かの所有する)犬と共に歩いています
	- 犬の所有格か示されていないため、誰の所有物か判らない

### 動詞に対して使う

再帰接頭辞 **su‐** は**動詞に対しても付ける事が出来ます**。殆どの場合、こちらの方が動作が明確になり、文も簡潔になります。

(訳注:所謂「屈折語」に近い表記。自然言語ならフィンランド語、人工言語ならヴォラピュクがこれに近い)

- ley <u>**su**safifi</u> = 彼らは<u>彼ら**自身**を洗った</u>
- done plesi loa maydane <u>**su**icifi</u> = 紳士は(彼の)メイドに<u>**彼に**(服を)着せる</u>よう頼んだ。

## 関係代名詞

他の単語を参照して新しい節(誰が、何を、どこで、どのように…)を導入する事が出来ます。先行詞と従属節はカンマで区切ります。

|関係代名詞|使う対象|対応する英語の関係代名詞|
|:-:|-|:-:|
|**e**|考え、状況|*what*|
|**a**|指定、呼称、品質|*which, the one, such as*|
|**o**|礼儀、作法、道、手段|*as, how*|
|**en**|位置、場所|*where*|
|**os**|時間、時制|*when*|
|**ke**|(汎用。カンマ不要)|*that*|

|例文|先行詞|従属節|例文の意味|
|-|-|-|-|
|cesí, **e** tu nudi|cesí<br>**取る**|tu nudi<br>あなたが必要なもの|あなたが必要なものを取る|
|welí, **e** me voli pravesi|welí<br>**見る**|me voli pravesi<br>私が買いたいもの|私が買いたいものを見る|
|welí, **a** me voli pravesi|welí<br>**見る**|me voli pravesi<br>私が買いたいもの|私が買いたいものを見る|
|mate, **a** me lovi|mate<br>女性|me lovi<br>私が愛している|私の愛する女性|
|ane, tem **a** me peli na tu|ane, tem<br>ある人、について|me peli na tu<br>私はあなたに話す|ある人について私はあなたに話す|
|me ami tu, **a** tu si|me ami tu<br>私はあなたを愛しています|tu si<br>あなたは|ありのままのあなたを愛しています|
|me i, **o** me voli|me i<br>私はする|me voli<br>私は望む|私は好きなようにします|
|eve, **en** me jiti|eve<br>家|me jiti<br>私は住んでいる|私の住んでいる家|
|hore, **os** tu tiiti|hore<br>時刻|tu tiiti<br>あなたは到着する|あなたが到着する時刻|

|ke を使った例文1|先行詞|従属節|例文の意味|
|-|-|-|-|
|myawe **ke** nyami nyameye|myawe<br>猫|nyami nyameye<br>食事をする|食事をする猫|
|balo **ke** depuci pilte en dome|balo<br>彼|depuci pilte en dome<br>自宅で絵を描く|自宅で絵を描く彼|
|mate **ke** me lovi|mate<br>女性|me lovi<br>私は愛します|私の愛する女性|
|ane **ke** me peli na tu|ane<br>ある人|me peli na tu<br>私はあなたに話します|私はあの人をあなたに話します?|
|eve **ke** me jiti|eve<br>家|me jiti<br>私は住んでいる|私の住んでいる家|
|hore **ke** tu tiiti|hore<br>時刻|tu tiiti<br>あなたは到着する|あなたが到着する時刻|

**ke** は複数の解釈が可能な場合があるため混乱しますが、大抵は文脈から判断する事が出来ます。

|ke を使った例文2|先行詞|従属節|例文の意味|
|-|-|-|-|
|cesí, **e** tu nudi|cesí<br>**取る**|tu nudi<br>あなたが必要なもの|あなたが必要なものを取る<br>あなたが必要なものを取れ?<br>あなたが必要な時に取る|
|me i, **ke** me voli|me i<br>私はする|me voli<br>私は望む|私は自分のしたい事をします<br>私は好きなようにします|
|ene **ke** me peli|ene<br>場所|me peli<br>私は話す|私が話している場所<br>私が話している所?|
|ene **ke** me vo pravesi|ene<br>場所|me vo pravesi<br>私は買おうとしている|私が買おうとしている場所<br>私が買おうとしている場所?|
