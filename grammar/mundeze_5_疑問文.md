# 文の形成

## 疑問文

文の最初か最後に疑問詞 **ki** または相関詞の一部を置く事で疑問文を作れます。これらの単語の前には前置詞を置く事も出来ます。

- **ki** balo nyami? / balo nyami **ki**? = 彼は食べます**か?**
	- ha, balo nyami = はい、彼は食べます
	- ne, balo guli = いいえ、彼は飲みます
- **kias** balo ne nyami? / lo ne nyami **kias**? = **何故**彼は食べないのですか?
	- as balo ne gwiri = 何故なら彼はお腹が空いていないからです
- **kie** tu nyami? = あなたは**何**を食べますか?
	- me nyami apole = 私はリンゴを食べます
- kon **kian** tu nyami? / tu nyami kon **kian**? = あなたは**誰**と食べますか?
	- me nyami kon mea basere = 私は私の兄弟と食べます
- me tsivoli **ki** malo nyami / me tsivoli, malo nyami **ki** = 彼女が食べるのでしょうか
- me tsivoli **kie** malo nyami / me tsivoli (ti) malo nyami **kie** = 彼女は何を食べているのでしょうか

### 曖昧さを回避する方法

疑問の対象は真偽の判断の出来る句の最初に置かれる事が多いですが、目的語を主語の前に置いたり、それについての疑問文を作る際に曖昧さをなくす目的で関係代名詞を使う事もあります。

- **ki** balo predio pravesi tie? / balo predio pravesi tie **ki**? = 彼は昨日それを買いました**か?**
- **ki** predio balo pravesi tie? / predio balo pravesi tie **ki**? = 彼がそれを買ったのは昨日です**か?**
- <u>tie</u> *ke* **balo predio pravesi** = **彼が昨日買った**<u>それ</u>
	- **ki** tie *ke* balo predio pravesi? / tie *ke* balo predio pravesi **ki**? = それは彼が昨日買ったのです**か?**
- <u>balo</u> *ke* **predio pravesi tie** = **昨日それを買った**<u>彼</u>
	- **ki** balo *ke* predio pravesi tie? / balo *ke* predio pravesi tie **ki**? = 彼は昨日それを買った人です**か?**

### 直前の疑問に対する補完や補足

**ki** は相互の疑問や直前の疑問を次の疑問の対象にする事も出来ます。

- me bono seti, <u>**ki** tu?</u> / me bono seti, <u>tu **ki**?</u> = 私は元気です。<u>あなたは?</u>
- kio tua mape seti? <u>**ki** tua bape?</u> / tua mape seti kio? <u>tua bape **ki**?</u> = あなたの母は元気ですか? <u>(そして、あなたの)父は(元気ですか)?</u>

## 否定と肯定

**ne**(いいえ) と **ha**(はい) を使う事で表します。

### 否定文と肯定文

否定したい対象の前に **ne** を置くだけです。逆に肯定を強調したい(疑惑を晴らすためにそれを証明したい)場合は **ha** を前に置くだけです。どちらも**何を対象にするかで文の意味が変わります。**

|否定文|意味|備考|
|-|-|-|
|me **ne** kanti|私は歌い**ません**|歌う事を拒否|
|**ne** me kanti|私は歌ってい**ません**<br>**いいえ**、私は歌います|**ne** に**イントネーションを**置く<br>**ne** で一度発音を区切る|
|**ne** me **ne** kanti|私は歌**わない**訳**ではありません**|二重否定|

|肯定文|意味|備考|
|-|-|-|
|me **ha** kanti|私は**本当に**歌います|歌う事を証明|
|**ha** me kanti|**本当に**私は歌っています<br>**はい**、私は歌います|**ha** に**イントネーションを**置く<br>**ha** で一度発音を区切る|

2つ目の例のような否定/肯定文は**イントネーションを変えます**。アクセントを変える訳ではないので注意して下さい。
二重否定は結果として肯定を意味しますが、意味が解り辛くなるため意図的な演出を除いて使わないで下さい。

否定文には相関詞を使う事も出来ます。

- tia kamere si **neya** = その部屋には**誰もいない**

### 否定疑問文

(相互)否定的な疑問に対しての返答は、疑問の対象になる単語に対してのみ **ha** と **ne** で答えます。これは英語と同じです。

- ki tu ne **gwiri**? = **お腹が空いて**いませんね?
	- ne = ne **gwiri** = はい(**お腹は空いて**いません)
	- ha = ha **gwiri** = いいえ(**お腹が空いて**います)

日本語とは反対のため、混乱するのであれば、**はい/いいえで答えず、文で答える**方が良いでしょう。

- ki tu ne **gwiri**? = **お腹が空いて**いませんね?
	- me ne **gwiri** = **お腹は空いて**いません
	- me **gwiri** = **お腹が空いて**います

### 注釈

肯定文と似ていますが、**ha** を文の一部を強調する目的で使う事も出来ます。

- malo bono kanti = 彼女は上手に歌います
	- malo **ha** bono kanti! = 彼女は**とても**上手に歌います!
- bela bate = ハンサムな男
	- **ha** bela bate! = **何て**ハンサムな男!

## 感嘆文

驚きを表すのに使い、文末のイントネーションを上げて発音します。

- tu duro ne fini!? = まだ終わってないの!?
- balo jo peli kie!? = 彼は何を言った!?
- tu idinavi!? = 妊娠したの!? / ご懐妊!?
- tu nomifi tie kapele!? = あなたはそれを帽子と呼ぶのですか!?
