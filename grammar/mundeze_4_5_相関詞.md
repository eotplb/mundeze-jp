# 相関詞

様々な動作や状態を表す機能語をまとめたものが相関詞です。これはエスペラントの相関詞から発想を得たもので、機能的にもほぼ同じものです。

初めから全ての相関詞を覚える必要はありません。必要に応じて覚えていけば十分です。

## 翻訳あり

|||質問|指示|無差別|不定|否定|全て|その他|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|||**KI‐**|**TI‐**|**PY‐**|**Y‐**|**NEY‐**|**OL‐**|**AL‐**|
|**物事、局面**|**‐E**|kie<br>**代名詞**<br>何|tie<br>**代名詞**<br>それ|pye<br>**副詞**<br>何でも|ye<br>**代名詞**<br>ある物|neye<br>**代名詞/形容詞**<br>何も～ない|ole<br>**代名詞**<br>全て|ale<br>**代名詞?**<br>他の何か|
|**確定、限定**|**‐A**|kia<br>**代名詞/形容詞**<br>誰、どの、どれ、どちら|tia<br>**代名詞/形容詞**<br>これ、この、その|pya<br>**副詞**<br>誰でも|ya<br>**代名詞/形容詞**<br>とある|neya<br>**代名詞/形容詞**<br>誰も～ない|ola<br>**代名詞/形容詞**<br>皆|ala<br>**形容詞**<br>他の|
|**様式、流儀**|**‐O**|kio<br>**副詞/接続詞**<br>どのように、～のように|tio<br>**代名詞/副詞**<br>あれ、あの、そのように|pyo<br>**副詞/接続詞**<br>何れにしても、何はともあれ|yo<br>**副詞**<br>どうにかして|neyo<br>**副詞**<br>どうにも～ない|olo<br>**副詞**<br>色々に|alo<br>**副詞**<br>他に|
|**原因、理由**|**‐AS**|kias<br>**副詞/接続詞**<br>何故、何故なら|tias<br>**副詞**<br>そのため|pyas<br>**副詞/接続詞**<br>どんな理由でも|yas<br>**副詞**<br>何かの理由で|neyas<br>**副詞**<br>どういう～でもない|olas<br>**副詞**<br>全てのために|alas<br>**副詞?**<br>他の理由|
|**時間、時制**|**‐OS**|kios<br>**副詞/接続詞**<br>いつ|tios<br>**副詞**<br>その時に|pyos<br>**副詞/接続詞**<br>いつでも|yos<br>**副詞**<br>その時|neyos<br>**副詞**<br>いつでも/決して～ない|olos<br>**副詞**<br>いつも|alos<br>**副詞**<br>その時以外|
|**個人、個体**|**‐AN**|kian<br>**代名詞/形容詞**<br>どの人|tian<br>**代名詞/形容詞**<br>あの人|pyan<br>**副詞**<br>どんな人でも|yan<br>**代名詞/形容詞**<br>ある人|neyan<br>**代名詞/形容詞**<br>どんな人でも～ない|olan<br>**副詞**<br>全ての人に|alan<br>**代名詞/形容詞**<br>他の人|
|**場所**|**‐EN**|kien<br>**副詞**<br>何処に|tien<br>**副詞**<br>そこに|pyen<br>**副詞/接続詞**<br>何処でも|yen<br>**代名詞/形容詞**<br>ある場所|neyen<br>**副詞**<br>何処にも～ない|olen<br>**副詞**<br>至る所|alen<br>**副詞**<br>他の場所|
|**量、数量**|**‐OM**|kiom<br>**副詞/接続詞**<br>いくつ|tiom<br>**副詞**<br>それだけ|pyom<br>**副詞**<br>いくつでも|yom<br>**副詞**<br>いくらか、少し|neyom<br>**副詞**<br>少しも～ない|olom<br>**副詞**<br>全部、すっかり|alom<br>**副詞**<br>他の全てに|

## 単語のみ

|||質問|指示|無差別|不定|否定|全て|その他|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|||**KI‐**|**TI‐**|**PY‐**|**Y‐**|**NEY‐**|**OL‐**|**AL‐**|
|**物事、局面**|**‐E**|kie|tie|pye|ye|neye|ole|ale|
|**確定、限定**|**‐A**|kia|tia|pya|ya|neya|ola|ala|
|**様式、流儀**|**‐O**|kio|tio|pyo|yo|neyo|olo|alo|
|**原因、理由**|**‐AS**|kias|tias|pyas|yas|neyas|olas|alas|
|**時間、時制**|**‐OS**|kios|tios|pyos|yos|neyos|olos|alos|
|**個人、個体**|**‐AN**|kian|tian|pyan|yan|neyan|olan|alan|
|**場所**|**‐EN**|kien|tien|pyen|yen|neyen|olen|alen|
|**量、数量**|**‐OM**|kiom|tiom|pyom|yom|neyom|olom|alom|

(訳注:例の意味が噛み合っていない気がする)

- **kian** tu (si)? = あなたは**どの人**ですか? = あなたは**誰**ですか?
	- me (si) silvano = 私はシルヴァーノです
- **kie** tu (si)? = あなたは**何**ですか?
	- me (si) bate = 私は男です
- kie **tie** (si)? = **それ**は何ですか?
	- tie (si) buke = それは本です
- **kia** buke tu voli? = あなたは**どの**本をお望みですか?
	- me voli **tia** buke = **その**本が欲しいです
- kia sorte buke si? = どんなジャンルの本ですか?
	- si preosotsa buke = 歴史書です
- **kio** tu seti? = **どのような**健康状態ですか = 元気?
	- me bono seti = 私は良い健康状態です = 元気です
- **kien** tu jiti? = **何処**に住んでいますか?
	- me jiti en kanade = カナダに住んでいます
