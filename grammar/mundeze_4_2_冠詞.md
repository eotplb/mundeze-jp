# 冠詞

**冠詞はありません**。対象が定まっているかどうかは文脈で表します。定まっていない場合は特定するような修飾語を使わずに表します。

- eve = 家
- tsipe = 鳥
- matey = 女性達

ある対象の中で1つを例に挙げたい場合は数詞の1である **un** を使います。

- **un** wawe = **ある**犬
- **un** batey = **ある**男達

数量が定まっていない複数の対象を表すには **ya**(いくつかの、一部の) を使います。

- **ya** stele = **いくつかの**星

文脈から全く対象や数量が特定出来ない場合、 **tia**(あれ) 等の形容詞を使えます。

- **tia** aney = **あの**人達

## 文例

- me si bate = 私は男です
- me si bate **ke** predio peli na tu = 私は男です + 昨日あなたに話しかけた = 私は昨日あなたに話しかけた男です
	- **ke** は関係代名詞で英語の *that, which* に相当
- tu si alta bate = あなたは背の高い男です
- me voli gua eve = 私は大きな家が欲しいです
- me ami tcokolate = 私はチョコレートが好きです
- me voli tcokolate = 私はチョコレートが欲しいです
- me voli **un** tcokolateye = 私は**ある**チョコレートが欲しいです
- mea masamia krome si rodje = 私のお気に入りの色は赤です
- agiley si tsipe = (それらの)鷲は鳥です
- me ami agiley = 私は鷲が好きです
- me peli na **ya** mate = 私は**一部の**女性に話しかけています
- ki tu peli na tsipey? = あなたは鳥と話しますか?
- ki tu peli na **ya** tsipey? = あなたは**一部の**鳥と話しますか?
